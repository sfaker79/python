from selenium import webdriver # Importation du module Selenium
from time import sleep # Importation du module time pour la fonction sleep
from selenium.webdriver.common.by import By # Importation du module By pour les éléments du navigateur
#from webdriver_manager.chrome import ChromeDriverManager # Importation du module ChromeDriverManager pour le driver Chrome
from selenium.webdriver.chrome.service import Service # Importation du module Service pour le service Chrome

#service1 = Service(ChromeDriverManager().install())  # Installation du driver Chrome
#driver = webdriver.Chrome(Service=service1)  # Création de l'objet bobby pour la navigateur chrome

driver = webdriver.Chrome(executable_path="chromedriver.exe")

###---------NAVIGATE TO phptravel---------###
driver.get("https://phptravels.org/") # Ouvre le navigateur et va sur le site

buttonRegister = driver.find_element(By.XPATH, "//a[@class='small font-weight-bold']")
buttonRegister.click()

Firstname = driver.find_element(By.ID, "inputFirstName")
Firstname.send_keys("Anes")

Lastname = driver.find_element(By.NAME, "lastname")
Lastname.send_keys("Faker")

Email = driver.find_elements(By.XPATH, "//input[@class='field form-control']")

for elem in Email:
    if (elem.get_attribute("name") == "email"):
        elem.send_keys("anes.faker@gmail.com")
        break

sleep(1000)  # Pause de 1000 sec