# YouTube/chaîne : Greggg
# class :   - Gestionnaire (utilisé 1 fois)
#           - Tache (utilisé pour chaque tache)
# Fonctionnalités :
# - Creer une tache
# - Supprimer une tache
# - Supprimer toutes les taches
# - Lister les taches
# - Sauvegarde des données

import pickle
import os

instruction = """
---------------------------------------------
|           Gestionnaire de tâches          |
|         Soufiane : 05/06/2023 | v1        |
|-------------------------------------------|
|   a Ranger = nouvelle tâche "Ranger"      |
|   s Ranger = supprime la tâche "Ranger"   |
|   st = supprime toutes les tâches         |
|   l = affiche la liste des tâches         |
|   i = re-affiche ces instructions         |
|   q = quitte l'app et sauvegarde          |
---------------------------------------------
"""

class TaskManager:
    def __init__(self):
        self.tasks = []

        if(os.path.exists("save.txt")):
            self.loadData()

    def saveData(self):
        with open('save.txt', 'wb') as saveFile:
            pickle.dump(self.tasks, saveFile)

    def loadData(self):
        with open('save.txt', 'rb') as saveFile:
            self.tasks = pickle.load(saveFile)

    def createTask(self, task):
        self.tasks.append(Task(task))

class Task:
    def __init__(self, task):
        self.task = task



print(instruction)
taskManager = TaskManager()

while True:
    print("")
    prompt = input("Action : ")
    if(prompt[:2] == "a "):
        newtask = prompt[2:]
        print("Ajouter la tâche '" + newtask + "' ? (oui/non)")
        prompt = input()
        if(prompt == "oui"):
            taskManager.createTask(newtask)

    elif(prompt[:2] == "s "):
        taskToDel = prompt[2:]
        taskToDelObject = None
        for element in taskManager.tasks:
            if(element.task == taskToDel):
                taskToDelObject = element

        if(taskToDelObject == None):
            print("Il n'y a pas de tâche nommer '" + taskToDel + "'")
        else:
            print("Supprimer la tâche '" + taskToDel + "' ? (oui/non)")
            prompt = input()
            if(prompt == "oui"):
                taskManager.tasks.remove(taskToDelObject)
    
    elif(prompt == "st"):
        print("Supprimer toute les tâches ? (oui/non)")
        prompt = input()
        if(prompt == "oui"):
            taskManager.tasks = []


    elif(prompt[0] == "l"):
        for element in taskManager.tasks:
            print(" - " + element.task)
    
    elif(prompt[0] == "i"):
        print(instruction)
    
    elif(prompt[0] == "q"):
        print("Quitter l'application ? (oui/non)")
        prompt = input()
        if(prompt == "oui"):
            taskManager.saveData()
            quit()
# print(taskManager)

# taskManager.createTask("ranger ma chambre")
# taskManager.createTask("faire la vaissele")
# print(taskManager.tasks)

# for element in taskManager.tasks:
    # print(element.task)


