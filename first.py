def carre(x):
    return x * x
    
print(carre(3))

carreLambda = lambda x : x*x
print(carreLambda(10))

mulLambda = lambda x, y : x * y
print(mulLambda(5,3))

print("------------------")

nums = [1, 2, 3, 4, 5, 6, 7, 8, 9]

carreMap = map(lambda x : x*x, nums)

for num in carreMap:
    print(num)
